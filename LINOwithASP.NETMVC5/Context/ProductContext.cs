﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data.Entity;
using LINOwithASP.NETMVC5.Models;

namespace LINOwithASP.NETMVC5.Context
{
    public class ProductContext :DbContext
    {
        public DbSet<Product> Products { get; set; }
    }
}